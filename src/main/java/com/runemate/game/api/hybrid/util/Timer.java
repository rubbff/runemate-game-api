package com.runemate.game.api.hybrid.util;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.util.calculations.*;
import java.util.concurrent.*;

/**
 * A utility class for counting down runtime. Runs from start to 0.
 * Can be paused, but must also be manually started.
 */
public final class Timer {
    private final long length;
    private long runtime;
    private long lastTime = -1;

    public Timer(final long length) {
        this.length = length;
    }

    public Timer(final long minimumLength, final long maximumLength) {
        this(Random.nextLong(minimumLength, maximumLength));
    }

    /**
     * Invokes the passed Callable and returns it's value, reporting the time in ms that it took to complete.
     *
     * @param name     The key that the timer will be reported against
     * @param callable The callable to invoke
     * @return the value returned by the callable
     */
    public static <T> T run(String name, Callable<T> callable) {
        T result = null;
        final long start = System.nanoTime();

        try {
            result = callable.call();
        } catch (Exception e) {
            e.printStackTrace();
        }

        final long duration = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start);
        Environment.getLogger().debug(String.format("Timed action (%s) took %sms", name, duration));
        return result;
    }

    /**
     * Gets the runtime in milliseconds
     */
    public long getRemainingTime() {
        return update() ? Math.max(length - runtime, 0) : length;
    }

    public double getPercentageOfTimeRemaining() {
        return (getRemainingTime() / (double) length) * 100;
    }

    /**
     * Gets the runtime formatted as HH:mm:ss (ex: 07:45:21) by calling Time.format(long) on the current runtime.
     * If the runtime is over a day, it's formatted as DD:HH:mm:ss instead.
     */
    public String getRemainingTimeAsString() {
        return Time.format(getRemainingTime());
    }

    public long getElapsedTime() {
        if (!update()) {
            return 0;
        }
        return Math.min(length, runtime);
    }

    public double getPercentageOfTimeElapsed() {
        return (getElapsedTime() / (double) length) * 100;
    }

    public String getElapsedTimeAsString() {
        return Time.format(getElapsedTime());
    }

    /**
     * Starts/resumes the Timer
     */
    public void start() {
        lastTime = System.currentTimeMillis();
    }

    /**
     * Stops/pauses the Timer
     */
    public void stop() {
        update();
        lastTime = -1;
    }

    /**
     * Resets the Timers elapsed time to 0ms
     */
    public void reset() {
        runtime = 0;
        if (isRunning()) {
            lastTime = System.currentTimeMillis();
        }
    }

    /**
     * Checks whether the Timer is running.
     * A timer is considered running if it has been started and the specified time hasn't elapsed.
     */
    public boolean isRunning() {
        return lastTime != -1 && getRemainingTime() > 0;
    }

    private boolean update() {
        if (lastTime == -1) {
            return false;
        }
        runtime += (System.currentTimeMillis() - lastTime);
        lastTime = System.currentTimeMillis();
        return true;
    }

    @Override
    public String toString() {
        return "[Timer: " + getRemainingTime() + ']';
    }
}
