package com.runemate.game.api.hybrid.local.hud.interfaces;

import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.script.*;

/**
 * <p>
 * Hybrid helper class for the "Enter amount:" dialog seen in banks, deposit boxes and elsewhere
 **/
public class EnterAmountDialog {

    private final static int OSRS_WIDGET = 162;

    private EnterAmountDialog() {
    }

    /**
     * Determines whether or not the "Enter amount:" dialog is open
     *
     * @return true if the dialog is open
     */
    public static boolean isOpen() {
        final InterfaceComponent ic = getTextEntryComponent();
        return ic != null && ic.isVisible();
    }

    /**
     * Enters the amount specified into the dialog box and delays until the action was successful
     *
     * @param amount amount to type
     * @return if successfully typed the amount, selected enter and confirmed the dialog has closed
     */
    public static boolean enterAmount(final int amount) {
        return enterAmount(amount, PlayerSense.getAsBoolean(PlayerSense.Key.USE_METRIC_FORMAT));
    }

    public static boolean enterAmount(final int amount, final boolean metric) {
        return enterAmount(metric ? StringFormat.metricFormat(amount) : String.valueOf(amount));
    }

    public static boolean enterAmount(final String amount) {
        return isOpen() && Keyboard.type(amount, true) &&
            Execution.delayWhile(EnterAmountDialog::isOpen, 1800, 2400);
    }

    private static InterfaceComponent getTextEntryComponent() {
        return Interfaces.newQuery().containers(OSRS_WIDGET)
            .textContains(
                "Enter amount:",
                "How many do you wish to buy?",
                "How many do you wish to sell?",
                "Set a price for each item:",
                "Enter amount (",
                "How many coins do you wish to stake?"
            )
            .grandchildren(false).types(InterfaceComponent.Type.LABEL)
            .results().first();
    }
}
