package com.runemate.game.api.hybrid.local;

import com.runemate.client.game.open.*;
import java.io.*;
import java.util.*;

public class Varcs {

    private Varcs() {
    }


    public static String getString(int id) {
        return OpenVarcs.getString(id);
    }

    public static Map<Integer, String> getStrings() {
        Map<Integer, Serializable> varcmap = getVarcs();
        Map<Integer, String> varcstrings = new HashMap<>(varcmap.size());
        for (int index = 0; index < varcmap.size(); ++index) {
            Serializable value = varcmap.get(index);
            if (value instanceof String) {
                varcstrings.put(index, (String) value);
            }
        }
        return varcstrings;
    }


    public static int getInt(int id) {
        return OpenVarcs.getInt(id);
    }

    public static Map<Integer, Integer> getInts() {
        Map<Integer, Serializable> varcmap = getVarcs();
        Map<Integer, Integer> varcints = new HashMap<>(varcmap.size());
        for (int index = 0; index < varcmap.size(); ++index) {
            Serializable value = varcmap.get(index);
            if (value instanceof Integer) {
                varcints.put(index, (Integer) value);
            }
        }
        return varcints;
    }


    private static Map<Integer, Serializable> getVarcs() {
        return OpenVarcs.getVarcs();
    }
}
