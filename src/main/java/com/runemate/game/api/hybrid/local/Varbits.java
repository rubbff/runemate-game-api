package com.runemate.game.api.hybrid.local;

import com.google.common.cache.*;
import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.api.hybrid.cache.loaders.*;
import com.runemate.game.cache.*;
import com.runemate.game.cache.file.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import javax.annotation.*;

public class Varbits {
    private static final VarbitLoader OSRS_LOADER = new VarbitLoader(false);
    private static final Cache<Integer, Varbit> OSRS_CACHE =
        CacheBuilder.newBuilder().expireAfterAccess(5, TimeUnit.MINUTES).build();

    /**
     * Loads the Varbit with the specified id
     */
    @Nullable
    public static Varbit load(int id) {
        if (id >= 0) {
            Varbit varbits = (OSRS_CACHE).getIfPresent(id);
            if (varbits != null) {
                return varbits;
            }
            try {
                CacheVarbit loaded =
                    (OSRS_LOADER).load(JS5CacheController.getLargestJS5CacheController(), 14, id);
                if (loaded != null) {
                    Varbit vb =
                        new Varbit(id, loaded.typeId, loaded.varIndex, loaded.msb, loaded.lsb);
                    (OSRS_CACHE).put(id, vb);
                    return vb;
                }
            } catch (final IOException ioe) {
                System.err.println(
                    "Unable to load Varbit at " + id + ": \"" + ioe.getMessage() + '"');
            }
        }
        return null;
    }

    /**
     * Loads all the Varbits into a list.
     */
    public static List<Varbit> loadAll() {
        Js5IndexFile archive = (JS5CacheController.getLargestJS5CacheController()).getIndexFile(2);
        if (archive != null) {
            Map<Integer, byte[]> entries = archive.getReferenceTable().load(14);
            if (entries != null) {
                ArrayList<Varbit> varbits = new ArrayList<>(entries.size());
                for (Map.Entry<Integer, byte[]> entry : entries.entrySet()) {
                    try {
                        CacheVarbit parsed =
                            (OSRS_LOADER).form(14, entry.getKey(), entry.getValue());
                        if (parsed != null) {
                            varbits.add(new Varbit(entry.getKey(), parsed.typeId, parsed.varIndex,
                                parsed.msb, parsed.lsb
                            ));
                        }
                    } catch (final IOException ioe) {
                        ioe.printStackTrace();
                        System.err.println("Unable to load Varbit at " + entry.getKey() + ": \"" +
                            ioe.getMessage() + '"');
                    }
                }
                return varbits;
            }
        }
        return Collections.emptyList();
    }
}
