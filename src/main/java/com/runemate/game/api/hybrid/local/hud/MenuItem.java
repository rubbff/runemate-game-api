package com.runemate.game.api.hybrid.local.hud;

import com.runemate.client.framework.open.*;
import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import java.awt.*;
import java.util.List;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;
import javafx.scene.canvas.*;
import org.jetbrains.annotations.*;

public class MenuItem implements Interactable, Renderable {
    private static final Interactable FAILED_TO_RESOLVE_TARGET_ENTITY = new Interactable() {
        @Override
        public boolean isVisible() {
            return false;
        }

        @Override
        public double getVisibility() {
            return 0;
        }

        @Override
        public boolean hasDynamicBounds() {
            return false;
        }

        @Override
        public InteractablePoint getInteractionPoint(Point origin) {
            return null;
        }

        @Override
        public boolean contains(Point point) {
            return false;
        }

        @Override
        public boolean click() {
            return false;
        }

        @Override
        public boolean interact(Pattern action, Pattern target) {
            return false;
        }

        @Override
        public String toString() {
            return "UNRESOLVED";
        }
    };
    private static final Set<Integer> OSRS_UNSUPPORTED_OPCODES = ConcurrentHashMap.newKeySet();
    private static final int HEADER_HEIGHT = 19;

    public final int opcode;
    protected final int index;
    private final String action;
    private final long arg0;
    private final int arg1;
    private final int arg2;
    private final boolean rs3;
    private final String target;
    private Interactable entity = null;

    public MenuItem(
        int index, String action, String target, int opcode, long arg0, int arg1,
        int arg2, boolean rs3
    ) {
        this.index = index;
        this.action = JagTags.remove(action);
        this.target = JagTags.remove(target);
        this.opcode = opcode;
        this.arg0 = arg0;
        this.arg1 = arg1;
        this.arg2 = arg2;
        this.rs3 = rs3;
    }

    public MenuItem(final OpenMenuItem item) {
        this(item.getIndex(), item.getAction(), item.getTarget(), item.getOpcode(), item.getArg0(),
            item.getArg1(), item.getArg2(), false
        );
    }

    @Nullable
    public final String getAction() {
        return action;
    }

    public InteractableRectangle getBounds() {
        return getBounds(Menu.getItems());
    }

    private InteractableRectangle getBounds(List<MenuItem> items) {
        final int index = items.indexOf(this);
        if (index != -1) {
            int menuItemCount = OpenMenu.getSize();
            if (menuItemCount > 0) {
                int itemHeight = (OpenMenu.getHeight() - HEADER_HEIGHT) / menuItemCount;
                int itemWidth = OpenMenu.getWidth();
                if (itemWidth > 0 && itemHeight > 1) {
                    return new InteractableRectangle(OpenMenu.getX(),
                        OpenMenu.getY() + HEADER_HEIGHT + (itemHeight * index), itemWidth,
                        itemHeight - 1
                    );
                }
            }
        }
        return null;
    }

    public final int getIndex() {
        return index;
    }

    @Nullable
    public final String getTarget() {
        return target;
    }

    public Interactable getTargetEntity() {
        if (entity == null) {
            entity = resolveTargetEntity();
        }
        return entity;
    }

    public final Type getTargetType() {
        return Type.getByOpcode(opcode);
    }

    @Override
    public int hashCode() {
        int result = Boolean.hashCode(rs3);
        result = 31 * result + Long.hashCode(arg0);
        result = 31 * result + Integer.hashCode(opcode);
        result = 31 * result + Integer.hashCode(arg1);
        result = 31 * result + Integer.hashCode(arg2);
        if (action != null) {
            result = 31 * result + action.hashCode();
        }
        if (target != null) {
            result = 31 * result + target.hashCode();
        }
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof MenuItem) {
            MenuItem item = (MenuItem) o;
            return Objects.equals(item.action, action) && Objects.equals(item.target, target)
                && item.opcode == opcode && item.arg0 == arg0 && item.arg1 == arg1
                && item.arg2 == arg2;
        }
        return false;
    }

    @Override
    public void render(final Graphics2D g2d) {
        final Rectangle bounds = getBounds();
        if (bounds != null) {
            g2d.drawRect(bounds.x, bounds.y, bounds.width, bounds.height);
        }
    }

    @Override
    public void render(GraphicsContext gc) {
        final Rectangle bounds = getBounds();
        if (bounds != null) {
            gc.strokeRect(bounds.x, bounds.y, bounds.width, bounds.height);
        }
    }

    private synchronized Interactable resolveTargetEntity() {
        if (opcode != -1 && !OSRS_UNSUPPORTED_OPCODES.contains(opcode)) {
            Type type = Type.getByOpcode(opcode);
            if (type == null) {
                OSRS_UNSUPPORTED_OPCODES.add(opcode);
                ClientAlarms.onClientWarn(
                    "Unsupported Menu Opcode",
                    "Revision=" + OpenClient.getRevision() + ", opcode=" + opcode + ", action="
                        + action + ", target=" + target + ", arg0=" + arg0 + ", arg1= " + arg1
                        + ", arg2=" + arg2
                );
            } else {
                try {
                    Interactable resolved = type.resolve(opcode, arg0, arg1, arg2);
                    if (resolved != null) {
                        return resolved;
                    }
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                    OSRS_UNSUPPORTED_OPCODES.add(opcode);
                    ClientAlarms.onClientWarn(
                        "Error resolving menu target",
                        "Revision=" + OpenClient.getRevision() + ", opcode=" + opcode
                            + ", action=" + action + ", target=" + target + ", arg0=" + arg0
                            + ", arg1= " + arg1 + ", arg2=" + arg2
                    );
                }
            }
        }
        return FAILED_TO_RESOLVE_TARGET_ENTITY;
    }

    public boolean targets(Interactable desiredTarget) {
        if (desiredTarget == null) {
            return false;
        }
        final Interactable actualTarget = getTargetEntity();
        if (actualTarget == FAILED_TO_RESOLVE_TARGET_ENTITY) {
            //Rather be safe than sorry...
            Environment.getLogger().debug(
                "[Interaction] Failed to resolve target entity, " + "but interacting anyway");
            return true;
        } else if (!rs3 && actualTarget instanceof InteractablePoint
            && desiredTarget instanceof Coordinate) {
            return desiredTarget.contains((InteractablePoint) actualTarget);
        } else if (actualTarget instanceof InterfaceComponent) {
            if (desiredTarget instanceof InterfaceComponent) {
                if (Objects.equals(actualTarget, desiredTarget)) {
                    return true;
                } else {
                    Rectangle entBounds = ((InterfaceComponent) actualTarget).getBounds();
                    if (entBounds == null) {
                        return false;
                    }
                    Rectangle intBounds = ((InterfaceComponent) desiredTarget).getBounds();
                    if (intBounds == null) {
                        return false;
                    }
                    return entBounds.equals(intBounds) || entBounds.contains(intBounds)
                        || intBounds.contains(entBounds);
                }
            } else if (desiredTarget instanceof SpriteItem) {
                int id = ((SpriteItem) desiredTarget).getId();
                if (Objects.equals(id, ((InterfaceComponent) actualTarget).getContainedItemId())) {
                    return true;
                }
                return ((InterfaceComponent) actualTarget).getChildren().parallelStream()
                    .anyMatch(ic -> ic.getContainedItemId() == id);
            } else {
                Environment.getLogger().debug(
                    "[DEBUG] Desired target was " + desiredTarget + " but actual target was "
                        + actualTarget);
            }
        } else {
            if (Objects.equals(actualTarget, desiredTarget)) {
                return true;
            } else if (Environment.isVerbose() && actualTarget.getClass()
                .equals(desiredTarget.getClass())) {
                Environment.getLogger().debug(
                    "Bad equals on type " + actualTarget.getClass().getSimpleName()
                        + " if identical objects (desired: " + desiredTarget + ", actual: "
                        + actualTarget + ").");
            }
        }
        return false;
    }

    @NotNull
    @Override
    public final String toString() {
        return "MenuItem{" + index + " : \"" + action + "\" : \"" + target + "\" : "
            + getTargetEntity() + "}";
    }

    @Override
    public double getVisibility() {
        return isVisible() ? 100 : 0;
    }

    @Override
    public boolean hasDynamicBounds() {
        return true;
    }

    @Override
    public boolean click() {
        return click(null);
    }

    @Override
    public boolean isVisible() {
        return index != -1 && (index == 0 || OpenMenu.isOpen());
    }

    @Override
    public InteractablePoint getInteractionPoint(Point origin) {
        final InteractableRectangle bounds = getBounds();
        return bounds != null ? bounds.getInteractionPoint(origin) : null;
    }


    @Override
    public boolean contains(Point point) {
        List<MenuItem> items = Menu.getItems();
        final int index = items.indexOf(this);
        if (index == -1) {
            return false;
        }
        boolean menuOpen = OpenMenu.isOpen();
        if (index == 0 && !menuOpen) {
            return true;
        }
        if (menuOpen) {
            Rectangle bounds = getBounds(items);
            return bounds != null && bounds.contains(point);
        }
        return false;
    }

    public boolean click(Interactable interactable) {
        final int index = getIndex();
        if (index == -1) {
            return false;
        }
        if (!OpenMenu.isOpen() && index == 0 && !Mouse.isMenuInteractionForced()) {
            return Mouse.click(Mouse.Button.LEFT);
        }
        if (OpenMenu.isOpen() || Menu.open()) {
            return Mouse.click(this, Mouse.Button.LEFT);
        }
        return false;
    }


    @Override
    public boolean interact(final Pattern action, final Pattern target) {
        if (action.matcher(getAction()).find() && target.matcher(getTarget()).find()) {
            return click();
        } else {
            final int size = OpenMenu.getSize();
            if (size == 1 && OpenMenu.isOpen()) {
                int height = OpenMenu.getHeight();
                if (height > 37) {
                    Environment.getLogger().warn(
                        "Closing the menu in response to a bug in the game where the menu is "
                            + "enlarged to a height of " + height
                            + " yet only contains the Cancel option.");
                    if (!Menu.close()) {
                        Environment.getLogger().warn(
                            "Failed to close the menu in response to the games enlarged menu bug.");
                    } else {
                        return interact(action, target);
                    }
                }
            }
        }
        return false;
    }

    public enum Type {
        GAME_OBJECT(
            new int[] { 1, 2, 3, 4, 5, 6, 1001, 1002 },
            //osrs arg0 is object id, arg1 is region x, arg2 is region y
            //rs3 arg0 is something similar, just with different shifts and masks
            (opcode, arg0, arg1, arg2) -> {
                Coordinate base = Region.getBase();
                if (base == null) {
                    return null;
                }
                return GameObjects.getLoadedOn(base.derive(arg1, arg2), obj -> {
                    long id = arg0;
                    GameObjectDefinition def = obj.getDefinition();
                    if (def == null) {
                        return false;
                    } else if (def.getId() == id) {
                        return true;
                    }
                    GameObjectDefinition local = def.getLocalState();
                    return local != null && local.getId() == id;
                }).first();
            }
        ),
        NPC(
            new int[] { 7, 8, 9, 10, 11, 12, 13, 1003, 2009, 2010, 2011, 2012, 2013, 3003 },
            //arg0 is uid/index
            (opcode, arg0, arg1, arg2) -> {
                return Npcs.getAt(arg0.intValue());
            }
        ), //If it's > 2000, then it's attack
        PLAYER(
            new int[] {
                14, 15, 44, 45, 46, 47, 48, 49, 50, 51, 2044, 2045, 2046, 2047, 2048, 2049,
                2051
            },
            //arg0 is uid/index
            //arg1 is flagX
            //arg2 is flagY
            (opcode, arg0, arg1, arg2) -> {
                //14 = use selected item on player
                //15 = use selected spell on player
                return Players.getAt(arg0.intValue() & 2047);
            }
        ),
        GROUND_ITEM(
            new int[] { 16, 17, 18, 19, 20, 21, 22, 1004 },
            //arg0 is item id, arg1 is region x, arg2 is region y
            (opcode, arg0, arg1, arg2) -> {
                Coordinate base = Region.getBase();
                if (base == null) {
                    return null;
                }
                return GroundItems.getLoadedOn(base.derive(arg1, arg2), arg0.intValue()).first();
            }
        ),
        WALK_HERE(
            new int[] { 23 },
            //osrs arg1 is mouse x, arg2 is mouse y
            //rs3 arg1 is region x, arg2 is region y
            (opcode, arg0, arg1, arg2) -> new InteractablePoint(arg1, arg2)
        ), //Specialized opcodes for buttons
        INTERFACE_BUTTON(
            new int[] { 24, 25, 26, 28, 29, 30 },
            //26 uses none
            //arg2=id for opcodes 24, 28, 29
            //arg2=id, arg1=descendant || -1 for opcodes 25, 30
            (Integer opcode, Long arg0, Integer arg1, Integer arg2) ->
                opcode == 24 || opcode == 26 || opcode == 28 || opcode == 29 || arg1 == -1 ?
                    OSRSInterfaces.getAt(arg2 >> 16, arg2 & 0xffff) :
                    OSRSInterfaces.getAt(arg2 >> 16, arg2 & 0xffff, arg1)
        ),
        SPRITE_GRID(
            new int[] { 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 1005 },
            (Integer opcode, Long arg0, Integer arg1, Integer arg2) -> {
                if (opcode == 31) {
                    //use item on item
                    //selected item = source item
                    //arg0 = target item id
                    //arg1 = target item table index
                    //arg2 = target item table interface id
                } else if (opcode == 32) {
                    //use spell on item
                    //selected spell = source spell
                    //arg0 = target item id
                    //arg1 = target item table index
                    //arg2 = target item table interface id
                } else if (opcode == 38) {
                    //select item (+ deselects spell so perhaps it's the target of opcode 31 & 32)
                    //selected item id = arg0
                    //selected item index = arg1
                    //selected item name = def(selected id).name
                    //interface id = arg2
                } else if (opcode == 1005) {
                    //examine item
                    //interface id = arg2
                    //interface element index = arg1
                    //item id = arg0
                } else if (opcode >= 33 && opcode <= 37) {
                    //item inventory action
                    //arg0 = target item id
                    //arg1 = target item table index
                    //arg2 = target item table interface id
                } else if (opcode >= 39 && opcode <= 43) {
                    //item table action
                    //arg0 = target item id
                    //arg1 = target item table index
                    //arg2 = target item table interface id
                }
                return Inventory.getItemIn(arg1);
            }
        ),
        INTERFACE(
            new int[] { 57, 58, 1007 },
            //opcode = 57 or 1007, arg0 = option_index
            //action_enabled = (cfg >> 1 + option_index & 1) != 0)
            //arg2 is interface uid, arg1 is child id if not -1
            (opcode, arg0, arg1, arg2) -> arg1 == -1 ?
                OSRSInterfaces.getAt(arg2 >> 16, arg2 & 0xffff) :
                OSRSInterfaces.getAt(arg2 >> 16, arg2 & 0xffff, arg1)
        ),
        COORDINATE(new int[] { 59 }, (opcode, arg0, arg1, arg2) -> null),
        CANCEL(
            new int[] { 1006 },
            //No interactable for a type like this
            (opcode, arg0, arg1, arg2) -> null
        ),
        WORLD_MAP_ICON(
            new int[] { 1008 },
            //not supported but has known type
            (opcode, arg0, arg1, arg2) -> null
        );

        private final int[] opcodes;
        private final QuadFunction<Integer, Long, Integer, Integer, Interactable> resolve;

        Type(int[] opcodes, QuadFunction<Integer, Long, Integer, Integer, Interactable> resolve) {
            this.opcodes = opcodes;
            Arrays.sort(this.opcodes);
            this.resolve = resolve;
        }

        @Nullable
        public static Type getByOpcode(int opcode) {
            if (opcode != -1) {
                for (Type item : values()) {
                    if (Arrays.binarySearch(item.opcodes, opcode) >= 0) {
                        return item;
                    }
                }
            }
            return null;
        }

        @Override
        public String toString() {
            return StringFormat.format(name(), "_", " ", StringFormat.FormatStyle.MIXED_CASE);
        }

        @Nullable
        public Interactable resolve(int opcode, long param0, int param1, int param2) {
            if (opcode == -1) {
                return null;
            }
            return resolve.apply(opcode, param0, param1, param2);
        }
    }
}
