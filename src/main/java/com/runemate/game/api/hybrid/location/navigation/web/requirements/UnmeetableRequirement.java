package com.runemate.game.api.hybrid.location.navigation.web.requirements;

import java.io.*;

public class UnmeetableRequirement extends WebRequirement implements SerializableRequirement {
    public UnmeetableRequirement() {
    }

    public UnmeetableRequirement(int protocol, ObjectInput stream) {
        deserialize(protocol, stream);
    }

    @Override
    public int getOpcode() {
        return 9;
    }

    @Override
    public boolean serialize(ObjectOutput stream) {
        return true;
    }

    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        return true;
    }

    @Override
    public boolean isMet0() {
        return false;
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof UnmeetableRequirement;
    }
}
