package com.runemate.game.api.hybrid.cache.configs;

import com.google.common.cache.*;
import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.api.hybrid.cache.loaders.*;
import com.runemate.game.internal.exception.*;
import com.runemate.game.cache.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import javax.annotation.*;

public class IdentityKits {
    private static final Cache<Integer, IdentityKit> OSRS_CACHE = CacheBuilder.newBuilder().maximumSize(100).expireAfterAccess(1, TimeUnit.MINUTES).build();
    private static final IdentityKitLoader RS3_LOADER = new IdentityKitLoader(Js5Cache.RS3Archives.JS5_CONFIG, true);
    private static final IdentityKitLoader OSRS_LOADER = new IdentityKitLoader(Js5Cache.OSRSArchives.JS5_CONFIG, false);

    /**
     * Gets a list of identity kits within the range of [first, last]
     */
    @Nonnull
    public static List<IdentityKit> load(final int first, final int last) {
        return load(first, last, null);
    }

    /**
     * Gets a list of identity kits within the range of [first, last] that are accepted by the filter
     */
    @Nonnull
    public static List<IdentityKit> load(final int first, final int last, final Predicate<IdentityKit> filter) {
        ArrayList<IdentityKit> definitions = new ArrayList<>(last - first + 1);
        for (int id = first; id <= last; ++id) {
            final IdentityKit definition = load(false, id);
            if (definition != null && (filter == null || filter.test(definition))) {
                definitions.add(definition);
            }
        }
        definitions.trimToSize();
        return definitions;
    }

    @Nullable
    private static IdentityKit load(final boolean cache, final int id) {
        return load(JS5CacheController.getLargestJS5CacheController(), false, cache, id);
    }

    @Nullable
    private static IdentityKit load(
        JS5CacheController cache, boolean rs3, final boolean storeInCache, final int id
    ) {
        if (id >= 0) {
            try {
                IdentityKit def = (OSRS_CACHE).getIfPresent(id);
                if (def != null) {
                    return def;
                }
                CacheIdentityKit bdef;
                if (rs3) {
                    bdef = RS3_LOADER.load(cache, 3, id);
                } else {
                    bdef = OSRS_LOADER.load(cache, 3, id);
                }
                if (bdef != null) {
                    def = bdef.extended();
                    if (storeInCache) {
                        (OSRS_CACHE).put(id, def);
                    }
                    return def;
                }
            } catch (final IOException ioe) {
                throw new UnableToParseBufferException("Unable to load identity kit for " + id + ": \"" + ioe.getMessage() + '"', ioe);
            }
        }
        return null;
    }

    /**
     * Gets the identity kit with the specified id
     *
     * @return The identity kit if available, otherwise null
     */
    public static IdentityKit load(final int id) {
        return load(true, id);
    }

    /**
     * Loads all identity kits
     */
    public static List<IdentityKit> loadAll() {
        return loadAll(null);
    }

    /**
     * Loads all identity kits that are accepted by the filter
     */
    public static List<IdentityKit> loadAll(final Predicate<IdentityKit> filter) {
        int quantity = OSRS_LOADER.getFiles(JS5CacheController.getLargestJS5CacheController(), 3).length;
        ArrayList<IdentityKit> identityKits = new ArrayList<>(quantity);
        for (int id = 0; id <= quantity; ++id) {
            final IdentityKit definition = load(false, id);
            if (definition != null && (filter == null || filter.test(definition))) {
                identityKits.add(definition);
            }
        }
        identityKits.trimToSize();
        return identityKits;
    }
}
