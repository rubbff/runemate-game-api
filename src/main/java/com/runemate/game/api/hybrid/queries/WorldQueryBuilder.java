package com.runemate.game.api.hybrid.queries;

import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.queries.results.*;
import java.util.*;
import java.util.concurrent.*;

public class WorldQueryBuilder
    extends QueryBuilder<WorldOverview, WorldQueryBuilder, WorldQueryResults> {
    private Boolean membersOnly;
    private Boolean lootshare;
    private Boolean quickChat;
    private Boolean tournament;
    private Boolean deadman;
    private Boolean lastManStanding;
    private Boolean pvp;
    private List<String> activities;

    public WorldQueryBuilder activity(String... activity) {
        return activity(Arrays.asList(activity));
    }

    public WorldQueryBuilder activity(List<String> activity) {
        this.activities = activity;
        return get();
    }

    public WorldQueryBuilder member() {
        this.membersOnly = true;
        return get();
    }

    public WorldQueryBuilder free() {
        this.membersOnly = false;
        return get();
    }

    public WorldQueryBuilder lootshare(boolean on) {
        this.lootshare = on;
        return get();
    }

    public WorldQueryBuilder tournament(boolean on) {
        this.tournament = on;
        return get();
    }

    public WorldQueryBuilder deadman(boolean on) {
        this.deadman = on;
        return get();
    }

    public WorldQueryBuilder lastManStanding(boolean on) {
        this.lastManStanding = on;
        return get();
    }

    public WorldQueryBuilder pvp(boolean on) {
        this.pvp = on;
        return get();
    }

    @Deprecated
    public WorldQueryBuilder selectable(boolean selectable) {
        return get();
    }

    public WorldQueryBuilder quickChat(boolean on) {
        this.quickChat = on;
        return get();
    }

    @Override
    public WorldQueryBuilder get() {
        return this;
    }

    @Override
    public Callable<List<? extends WorldOverview>> getDefaultProvider() {
        return () -> Worlds.getLoaded().asList();
    }

    @Override
    public boolean accepts(WorldOverview overview) {
        if (activities != null) {
            boolean found = false;
            String thisActivity = overview.getActivity();
            for (String activity : activities) {
                if (activity.equals(thisActivity)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                return false;
            }
        }
        if (membersOnly != null
            && membersOnly != overview.isMembersOnly()) {
            return false;
        }
        if (lootshare != null
            && lootshare != overview.isLootShare()) {
            return false;
        }
        if (quickChat != null
            && quickChat != overview.isQuickChat()) {
            return false;
        }
        if (tournament != null
            && tournament != overview.isTournament()) {
            return false;
        }
        if (deadman != null
            && deadman != overview.isDeadman()) {
            return false;
        }
        if (lastManStanding != null
            && lastManStanding != overview.isLastManStanding()) {
            return false;
        }
        if (pvp != null
            && pvp != overview.isPVP()) {
            return false;
        }
        return super.accepts(overview);
    }

    @Override
    protected WorldQueryResults results(
        Collection<? extends WorldOverview> entries,
        ConcurrentMap<String, Object> cache
    ) {
        return new WorldQueryResults(entries, cache);
    }
}
