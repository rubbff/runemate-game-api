package com.runemate.game.api.hybrid.local;

import com.runemate.game.api.script.annotations.*;

/**
 * Information of the currently logged-in player's account.
 */
public class AccountInfo {

    @OSRSOnly
    public static int getDaysOfMembershipRemaining() {
        return Varps.getAt(1780).getValue();
    }

    @OSRSOnly
    public static boolean isMember() {
        return Varcs.getInt(103) == 1;
    }

    /**
     * Gets the account type associated with the currently logged-in player.
     *
     * @return the associated AccountType.
     */
    public static AccountType getType() {
        switch (Varps.getAt(499).getValue() >> 3 & 0x3) {
            case 0:
                return AccountType.REGULAR;
            case 1:
                return AccountType.IRONMAN;
            case 2:
                return AccountType.ULTIMATE_IRONMAN;
            default:
                return AccountType.HARDCORE_IRONMAN;
        }
    }

    /**
     * Known account types.
     */
    public enum AccountType {
        REGULAR,
        IRONMAN,
        ULTIMATE_IRONMAN,
        HARDCORE_IRONMAN
    }
}
