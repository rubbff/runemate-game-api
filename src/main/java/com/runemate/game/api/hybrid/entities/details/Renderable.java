package com.runemate.game.api.hybrid.entities.details;

import java.awt.*;
import javafx.scene.canvas.*;
import javax.annotation.*;

@FunctionalInterface
public interface Renderable {
    static double[] convert(@Nonnull int[] ints) {
        double[] doubles = new double[ints.length];
        for (int i = 0; i < ints.length; i++) {
            doubles[i] = ints[i];
        }
        return doubles;
    }

    /**
     * Renders this entity onto the given {@link Graphics2D}, use is discouraged
     */
    default void render(final Graphics2D g2d) {

    }

    /**
     * Renders this entity onto the given {@link GraphicsContext}
     */
    void render(final GraphicsContext gc);
}
