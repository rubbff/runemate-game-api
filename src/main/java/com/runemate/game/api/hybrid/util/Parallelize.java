package com.runemate.game.api.hybrid.util;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.script.framework.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.stream.*;

public class Parallelize {
    public static final int MAX_PARALLELISM = 16;
    public static final ConcurrentMap<AbstractBot, ForkJoinPool> BOT_FORK_JOIN_POOLS = new ConcurrentHashMap<>();

    public static <T> List<T> collectToList(Collection<T> collection, Predicate<T> callable) {
        if (callable == null) {
            return collection instanceof List ? (List<T>) collection : new ArrayList<>(collection);
        }
        AbstractBot bot = Environment.getBot();
        if (bot == null) {
            return collection instanceof List ? (List<T>) collection : new ArrayList<>(collection);
        }
        try {
            Stream<T> stream = collection.parallelStream().filter(callable);
            return BOT_FORK_JOIN_POOLS.computeIfAbsent(
                    bot, abot -> new ForkJoinPool(Math.min(MAX_PARALLELISM, Runtime.getRuntime().availableProcessors())))
                .submit(() -> stream.collect(Collectors.toList()))
                .get();
        } catch (InterruptedException | ExecutionException | CancellationException e) {
            Environment.getLogger().debug("Parallel collect task were cancelled");
            return new ArrayList<>(0);
        }
    }

    public static <T, R> List<R> mapAndCollectToList(
        Collection<T> collection, Function<? super T, ? extends R> mapper
    ) {
        if (collection == null) {
            return null;
        }
        final AbstractBot bot = Environment.getBot();
        if (bot == null) {
            return null;
        }
        try {
            return BOT_FORK_JOIN_POOLS.computeIfAbsent(
                bot, _bot -> new ForkJoinPool(Math.min(MAX_PARALLELISM, Runtime.getRuntime().availableProcessors()))).submit(() -> {
                final Stream<R> stream = collection.parallelStream().map(mapper);
                return stream.collect(Collectors.toList());
            }).get();
        } catch (InterruptedException | ExecutionException | CancellationException e) {
            Environment.getLogger().debug("Parallel collect task were cancelled");
            return Collections.emptyList();
        }
    }

    public static <T> Set<T> collect(Collection<T> collection, Predicate<T> callable) {
        if (callable == null) {
            return collection instanceof Set ? (Set<T>) collection : new HashSet<>(collection);
        }
        AbstractBot bot = Environment.getBot();
        if (bot == null) {
            return collection instanceof Set ? (Set<T>) collection : new HashSet<>(collection);
        }
        try {
            Stream<T> stream = collection.parallelStream().filter(callable);
            return BOT_FORK_JOIN_POOLS.computeIfAbsent(
                    bot, _bot -> new ForkJoinPool(Math.min(MAX_PARALLELISM, Runtime.getRuntime().availableProcessors())))
                .submit(() -> stream.collect(Collectors.toSet()))
                .get();
        } catch (InterruptedException | ExecutionException | CancellationException e) {
            Environment.getLogger().debug("Parallel collect task were cancelled");
            return Collections.emptySet();
        }
    }

    public static <T> T findFirst(Collection<T> collection, Predicate<T> callable) {
        if (collection.isEmpty()) {
            return null;
        }
        if (callable == null) {
            return collection.iterator().next();
        }
        AbstractBot bot = Environment.getBot();
        if (bot == null) {
            return collection.iterator().next();
        }
        try {
            Stream<T> stream = collection.parallelStream().filter(callable);
            return BOT_FORK_JOIN_POOLS.computeIfAbsent(
                    bot, _bot -> new ForkJoinPool(Math.min(MAX_PARALLELISM, Runtime.getRuntime().availableProcessors())))
                .submit(() -> stream.findFirst().orElse(null))
                .get();
        } catch (InterruptedException | ExecutionException | CancellationException e) {
            Environment.getLogger().debug("Parallel findFirst task was cancelled");
            return null;
        }
    }

    public static <T, R> Set<R> mapAndCollect(
        Collection<T> collection, Function<? super T, ? extends R> mapper
    ) {
        if (collection == null) {
            return null;
        }
        final AbstractBot bot = Environment.getBot();
        if (bot == null) {
            return null;
        }
        try {
            return BOT_FORK_JOIN_POOLS.computeIfAbsent(
                    bot, _bot -> new ForkJoinPool(Math.min(MAX_PARALLELISM, Runtime.getRuntime().availableProcessors())))
                .submit(() -> collection.parallelStream().<R>map(mapper).collect(Collectors.toSet()))
                .get();
        } catch (InterruptedException | ExecutionException | CancellationException e) {
            Environment.getLogger().debug("Parallel collect task were cancelled");
            return Collections.emptySet();
        }
    }
}
