package com.runemate.game.api.osrs.region;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.osrs.entities.*;
import com.runemate.game.api.osrs.local.hud.*;
import java.util.*;
import java.util.function.*;

public final class OSRSNpcs {
    private OSRSNpcs() {
    }


    public static LocatableEntityQueryResults<Npc> getLoaded(final Predicate<? super Npc> filter) {
        ArrayList<Npc> npcs = new ArrayList<>(64);
        long[] npcUids = OpenNpc.getLoadedDirty();
        if (npcUids != null) {
            int[] npcIndices = OpenNpc.getNpcIndices();
            for (final int npc_index : npcIndices) {
                long npcUid = npcUids[npc_index];
                if (npcUid != 0) {
                    final Npc npc = new OSRSNpc(npcUid);
                    if (filter == null || filter.test(npc)) {
                        npcs.add(npc);
                    }
                }
            }
        }
        npcs.trimToSize();
        return new LocatableEntityQueryResults<>(npcs);
    }


    public static OSRSNpc getByIndex(final int index) {
        //The arrays size is 32768
        final long npc_uid = OpenNpc.getNpcByIndex(index);
        return npc_uid != 0 ? new OSRSNpc(npc_uid) : null;
    }


    public static OSRSModel lookupModel(final int id, final LocatableEntity entity) {
        long modeluid = OpenNpc.lookupNpcModel(id);
        return modeluid != 0 ? new OSRSModel(modeluid, entity) : null;
    }
}
