package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.queries.*;

public final class LootingBag {
    private LootingBag() {
    }

    public static boolean isOpen() {
        return Inventories.opened(Inventories.Documented.LOOTING_BAG);
    }

    public static SpriteItemQueryBuilder newQuery() {
        return new SpriteItemQueryBuilder(Inventories.Documented.LOOTING_BAG);
    }
}
