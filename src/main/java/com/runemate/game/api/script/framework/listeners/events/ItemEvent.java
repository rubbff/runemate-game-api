package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.local.hud.interfaces.*;

public class ItemEvent implements Event {
    private final int change;
    private final SpriteItem item;

    public ItemEvent(final SpriteItem item, final int change) {
        this.item = item;
        this.change = change;
    }

    public SpriteItem getItem() {
        return item;
    }

    public int getQuantityChange() {
        return Math.abs(change);
    }

    public Type getType() {
        if (change > 0) {
            return Type.ADDITION;
        }
        if (change < 0) {
            return Type.REMOVAL;
        }
        return Type.UNKNOWN;
    }

    @Override
    public String toString() {
        return String.format("ItemEvent(quantity={%s}, item={\"%s\"})", change, item);
    }

    public enum Type {
        ADDITION,
        REMOVAL,
        UNKNOWN
    }
}
