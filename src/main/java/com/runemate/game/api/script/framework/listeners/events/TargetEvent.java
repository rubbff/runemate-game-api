package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.entities.details.*;
import java.util.concurrent.*;

public class TargetEvent implements Event {
    private final Callable<Animable> sourceLoader, targetLoader;

    public TargetEvent(Callable<Animable> sourceLoader, Callable<Animable> targetLoader) {
        this.sourceLoader = sourceLoader;
        this.targetLoader = targetLoader;
    }

    public Animable getSource() {
        try {
            return sourceLoader.call();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Animable getTarget() {
        try {
            return targetLoader.call();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String toString() {
        Animable source = getSource();
        Animable target = getTarget();
        return (source instanceof Onymous ? ((Onymous) source).getName() : source)
            + " is now targeting " + (
            target instanceof Onymous ? ((Onymous) target).getName() :
                target
        );
    }
}
