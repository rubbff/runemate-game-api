package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.client.framework.open.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.local.hud.*;
import java.util.*;

public class MenuInteractionEvent implements Event {
    private final int param1;
    private final int param2;
    private final int opcode;
    private final String action;
    private final String target;
    private final int mouseX;
    private final int mouseY;
    private final long param0;
    private MenuItem.Type type;
    private Interactable targetEntity;

    public MenuInteractionEvent(
        int param1, int param2, int opcode, long param0, String action,
        String target, int mouseX, int mouseY
    ) {
        this.param1 = param1;
        this.param2 = param2;
        this.opcode = opcode;
        this.param0 = param0;
        this.action = action;
        this.target = target;
        this.mouseX = mouseX;
        this.mouseY = mouseY;
    }

    public MenuItem.Type getTargetType() {
        if (type != null) {
            return type;
        }
        return type = MenuItem.Type.getByOpcode(opcode);
    }

    public Interactable getTargetEntity() {
        if (targetEntity != null) {
            return targetEntity;
        } else {
            return (targetEntity = getTargetType().resolve(opcode, param0, param1, param2));
        }
    }

    public int getParam1() {
        return param1;
    }

    public int getParam2() {
        return param2;
    }

    public int getOpcode() {
        return opcode;
    }

    public long getParam0() {
        return param0;
    }

    public String getAction() {
        return action;
    }

    public String getTarget() {
        return target;
    }

    public int getMouseX() {
        return mouseX;
    }

    public int getMouseY() {
        return mouseY;
    }

    @Deprecated
    public MenuItem.Type getType() {
        return getTargetType();
    }

    @Deprecated
    public String getOption() {
        return getAction();
    }

    @Deprecated
    public String getTitle() {
        return getTarget();
    }

    @Override
    public String toString() {
        StringJoiner join =
            new StringJoiner(", ", MenuInteractionEvent.class.getSimpleName() + "[", "]")
                .add("action='" + action + "'")
                .add("target='" + target + "'")
                .add("param0=" + param0)
                .add("param1=" + param1)
                .add("param2=" + param2)
                .add("opcode=" + opcode)
                .add("type=" + getTargetType())
                .add("mouseX=" + mouseX)
                .add("mouseY=" + mouseY);
        if (BotControl.isBotThread()) {
            join.add("targetEntity=" + getTargetEntity());
        }
        return join.toString();
    }
}
