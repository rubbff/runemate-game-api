package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.entities.details.*;
import java.util.concurrent.*;

public class AnimationEvent implements Event {
    private final Type type;
    private final Callable<Animable> sourceLoader;
    private final int animationId;

    public AnimationEvent(Type type, Callable<Animable> sourceLoader, int animationId) {
        this.type = type;
        this.sourceLoader = sourceLoader;
        this.animationId = animationId;
    }

    public Animable getSource() {
        try {
            return sourceLoader.call();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public int getAnimationId() {
        return animationId;
    }

    @Override
    public String toString() {
        Animable source = getSource();
        return "[" + type.name() + "] "
            + (source instanceof Onymous ? ((Onymous) source).getName() : source)
            + " has changed its animation to " + animationId;
    }

    public enum Type {
        NPC,
        PLAYER,
        GAME_OBJECT,
        SPOT_ANIMATION,
        PROJECTILE
    }
}
