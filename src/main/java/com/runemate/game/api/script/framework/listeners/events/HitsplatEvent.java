package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.status.*;
import java.util.concurrent.*;

public class HitsplatEvent implements Event {
    private final Callable<Actor> sourceLoader;
    private final Hitsplat hitsplat;

    public HitsplatEvent(Callable<Actor> sourceLoader, Hitsplat hitsplat) {
        this.sourceLoader = sourceLoader;
        this.hitsplat = hitsplat;
    }

    public Actor getSource() {
        try {
            return sourceLoader.call();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Hitsplat getHitsplat() {
        return hitsplat;
    }

    @Override
    public String toString() {
        Actor source = getSource();
        return "HitsplatEvent{"
            + (source instanceof Npc ? "npc" : "player") + "=" + source.getName()
            + ", damage=" + hitsplat.getDamage() + '}';
    }
}
