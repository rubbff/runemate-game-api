package com.runemate.game.api.script.framework.listeners.dispatchers;

import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.script.framework.core.*;
import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;

public class VarcDispatcher extends IngameEventDispatcher {
    private Map<Integer, Integer> ints;
    private Map<Integer, String> strings;

    @Override
    public int getIterationRateInMilliseconds() {
        return 100;
    }

    @Override
    public void dispatch(EventDispatcher dispatcher) {
        if (strings == null || ints == null) {
            strings = Varcs.getStrings();
            ints = Varcs.getInts();
        } else {
            Map<Integer, Integer> newVarcints = Varcs.getInts();
            for (int index = 0; index < ints.size() && index < newVarcints.size(); ++index) {
                int oldValue = ints.get(index);
                int newValue = newVarcints.get(index);
                if (oldValue != newValue) {
                    dispatcher.dispatchLater(new VarcEvent(false, index, oldValue, newValue));
                }
            }
            ints = newVarcints;
            Map<Integer, String> newVarcstrings = Varcs.getStrings();
            for (int index = 0; index < strings.size() && index < newVarcstrings.size(); ++index) {
                String oldValue = strings.get(index);
                String newValue = newVarcstrings.get(index);
                if (!Objects.equals(oldValue, newValue)) {
                    dispatcher.dispatchLater(new VarcEvent(true, index, oldValue, newValue));
                }
            }
            strings = newVarcstrings;
        }
    }

    @Override
    public void clear() {
        strings = null;
        ints = null;
    }
}
