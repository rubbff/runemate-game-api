package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.entities.*;

public class PlayerMovementEvent implements Event {
    private final Player player;

    public PlayerMovementEvent(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    @Override
    public String toString() {
        return "PlayerMovementEvent{player=" + player + "}";
    }
}
