package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.local.*;

public class VarbitEvent implements Event {
    private final Varbit varbit;
    private final int oldValue;
    private final int newValue;

    public VarbitEvent(Varbit varbit, int oldValue, int newValue) {
        this.varbit = varbit;
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    public Varbit getVarbit() {
        return varbit;
    }

    public int getOldValue() {
        return oldValue;
    }

    public int getNewValue() {
        return newValue;
    }

    public int getChange() {
        return newValue - oldValue;
    }

    @Override
    public String toString() {
        return "VarbitEvent{varbit=" + varbit.getId() + ", " + oldValue + " -> " + newValue + "}";
    }
}
