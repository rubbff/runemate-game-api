package com.runemate.game.internal.rmi;

import com.runemate.client.framework.open.*;
import com.runemate.commons.internal.scene.entities.objects.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.*;
import org.jetbrains.annotations.*;

public class BridgeServiceImpl implements BridgeService {

    private final Map<Integer, List<SpriteItem>> itemState = new ConcurrentHashMap<>();
    private final Map<Skill, int[]> skillState = new ConcurrentHashMap<>();

    private final AbstractBot bot;

    public BridgeServiceImpl(AbstractBot bot) {
        this.bot = bot;
    }


    @Nullable
    @Override
    public MinimalObjectDefinition getMinimalObjectDefinition(int i) {
        GameObjectDefinition definition = GameObjectDefinition.get(i);
        if (definition == null) {
            return null;
        }
        return new MinimalObjectDefinition(definition.getId(), definition.getClippingType(),
            definition.impassable(), definition.impenetrable()
        );

    }

    @Override
    public void initSkills() {
        if (skillState.size() == 0) {
            int[] experiences = Skills.getExperiences();
            for (int skillIndex = 0; skillIndex < experiences.length; skillIndex++) {
                Skill skill = Skills.getByIndex(skillIndex);
                if (skill == null) {
                    bot.getLogger()
                        .warn("Unable to initialize the state of a skill at index " + skillIndex);
                    continue;
                }
                int experience = experiences[skillIndex];
                int baseLevel = Skills.getLevelAtExperience(skill, experience);
                skillState.put(skill, new int[] { experience, baseLevel });
            }
        }

    }

    @Override
    public void processItemEventsFromChange(
        long invId, int itemIndex, int itemid,
        int itemQuantity
    ) {
        for (ItemEvent event : getItemEventsFromChange((int) invId, itemIndex, itemid,
            itemQuantity
        )) {
            bot.getEventDispatcher().dispatchLater(event);
        }
    }

    @Override
    public void processSkillEventsFromChange(int index, int experience, int currentLevel) {
        final Skill skill = Skills.getByIndex(index);
        if (skill == null) {
            return;
        }
        final int baseLevel = Skills.getLevelAtExperience(skill, experience);
        for (final SkillEvent event : getSkillEventsFromChange(skill, baseLevel, experience)) {
            bot.getEventDispatcher().dispatchLater(event);
        }
    }


    public List<SkillEvent> getSkillEventsFromChange(Skill skill, int level, int experience) {
        List<SkillEvent> events = new ArrayList<>(2);
        int[] skillData = skillState.get(skill);
        if (skillData == null) {
            skillState.put(skill, new int[] { experience, level });
        } else {
            if (skillData[0] < experience) {
                events.add(new SkillEvent(skill, SkillEvent.Type.EXPERIENCE_GAINED, experience,
                    skillData[0]
                ));
            }
            if (skillData[1] < level) {
                events.add(
                    new SkillEvent(skill, SkillEvent.Type.LEVEL_GAINED, level, skillData[1]));
            }
            skillData[0] = experience;
            skillData[1] = level;
        }
        return events;
    }

    public List<ItemEvent> getItemEventsFromChange(
        int inventoryId, int itemIndex, int itemId,
        int itemQuantity
    ) {
        List<ItemEvent> events = new ArrayList<>(2);
        List<SpriteItem> inventory = itemState.get(inventoryId);
        if (inventory == null) {
            itemState.put(inventoryId, inventory = Inventories.lookup(inventoryId).asList());
        }
        List<SpriteItem> itemsInSlot =
            inventory.stream().filter(item -> item.getIndex() == itemIndex)
                .collect(Collectors.toList());
        if (itemsInSlot.size() > 1) {
            bot.getLogger().severe("The inventory with id " + inventoryId +
                " cannot have more than one item in index " + itemIndex);
        }
        SpriteItem itemInSlot = !itemsInSlot.isEmpty() ? itemsInSlot.get(0) : null;
        if (itemInSlot == null && itemId >= 0 && itemQuantity > 0) {
            //Add item to empty slot
            SpriteItem item = new SpriteItem(itemId, itemQuantity, itemIndex,
                Inventories.Documented.getOrigin(inventoryId)
            );
            inventory.add(item);
            events.add(new ItemEvent(item, itemQuantity));
        }
        if (itemInSlot != null) {
            if (itemInSlot.getId() == itemId) {
                //Update quantity of item in slot
                int stackChange = itemQuantity - itemInSlot.getQuantity();
                if (stackChange != 0) {
                    //but only bother sending an event and updating the cache if the quantity actually changed
                    SpriteItem adjustedStackItemInSlot = itemInSlot.derive(stackChange);
                    inventory.remove(itemInSlot);
                    inventory.add(adjustedStackItemInSlot);
                    events.add(new ItemEvent(adjustedStackItemInSlot, stackChange));
                }
            } else {
                //Replace an item in an occupied slot
                //1. Remove from cache and send event negativing it's entire presence
                //2. Build new item in it's slot, cache it, and fire added event.
                inventory.remove(itemInSlot);
                events.add(new ItemEvent(itemInSlot, -itemInSlot.getQuantity()));
                if (itemId != -1) {
                    SpriteItem newItemInSlot = new SpriteItem(itemId, itemQuantity, itemIndex,
                        Inventories.Documented.getOrigin(inventoryId)
                    );
                    inventory.add(newItemInSlot);
                    events.add(new ItemEvent(newItemInSlot, itemQuantity));
                }
            }
        }
        return events;
    }

}
