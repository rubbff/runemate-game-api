package com.runemate.bot.test;

import com.runemate.game.api.script.framework.*;
import java.util.*;

/**
 * This bot can be used to test new features that you're working on.Recommend that you use this to make it obvious to the reviewer
 * how exactly the code change was tested.
 *
 * The "testJar" gradle task will produce a jar containing this bot which you can place in a bot directory.
 */
public class FeatureTestBot extends LoopingBot {

    @Override
    public void onStart(final String... arguments) {
        if (this instanceof EventListener) {
            getEventDispatcher().addListener((EventListener) this);
        }
    }

    @Override
    public void onLoop() {

    }
}
